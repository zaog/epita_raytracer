CXX=g++
CXXFLAGS=-Wall -Wextra -lpthread -std=c++11

SRC_DIR=src/

SRC=${wildcard $(SRC_DIR)*.cpp}
SRC+=${wildcard $(SRC_DIR)Parser/*.cpp}
#SRC:=$(filter-out $(SRC_DIR)main.cc, $(SRC))

OBJ=${SRC:.cpp=.o}

#LIB_DIR=-L/usr/local/lib
LIB=-lgcc_s -lstdc++ -lgcc

TARGET=RayTracer

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJ) $(LIB_DIR) $(LIB) 

#$(SRC_DIR)main.o: $(SRC_DIR)main.cc

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	$(RM) $(OBJ) $(TARGET)
