#ifndef VECTOR3_HH
# define VECTOR3_HH

#include <cmath>
#include <iostream>

class Vector3
{
    public:
        inline Vector3() : _x(0), _y(0), _z(0) {}
        inline Vector3(double x, double y, double z) : _x(x), _y(y), _z(z) {}

        inline const double& GetX() const { return this->_x; }
        inline const double& GetY() const { return this->_y; }
        inline const double& GetZ() const { return this->_z; }
        inline const double& GetCell(int i) const { return _cell[i]; }
        inline void SetCell(int i, double value) { _cell[i] = value; }
        inline Vector3& Set(const double& x, const double& y, const double& z) { this->_x = x; this->_y = y; this->_z = z; return *this; }

        inline Vector3& operator=(const Vector3& other) { return this->Set(other.GetX(), other.GetY(), other.GetZ()); }

        inline Vector3& operator+=(const Vector3& b) { this->_x += b.GetX(); this->_y += b.GetY(); this->_z += b.GetZ(); return *this; }
        inline Vector3& operator-=(const Vector3& b) { this->_x -= b.GetX(); this->_y -= b.GetY(); this->_z -= b.GetZ(); return *this; }

        inline Vector3& operator+=(const double& b) { this->_x += b; this->_y += b; this->_z += b; return *this; }
        inline Vector3& operator-=(const double& b) { this->_x -= b; this->_y -= b; this->_z -= b; return *this; }
        inline Vector3& operator*=(const double& b) { this->_x *= b; this->_y *= b; this->_z *= b; return *this; }
        inline Vector3& operator/=(const double& b) { this->_x /= b; this->_y /= b; this->_z /= b; return *this; }

        inline Vector3 operator+(const Vector3& b) const { return Vector3(this->_x + b.GetX(), this->_y + b.GetY(), this->_z + b.GetZ()); }
        inline Vector3 operator-(const Vector3& b) const { return Vector3(this->_x - b.GetX(), this->_y - b.GetY(), this->_z - b.GetZ()); }

        inline Vector3 mul(const Vector3& b) const { return Vector3(this->_x * b.GetX(), this->_y * b.GetY(), this->_z * b.GetZ()); }

        inline Vector3 operator+(const double& b) const { return Vector3(this->_x + b, this->_y + b, this->_z + b); }
        inline Vector3 operator-(const double& b) const { return Vector3(this->_x - b, this->_y - b, this->_z - b); }
        inline Vector3 operator*(const double& b) const { return Vector3(this->_x * b, this->_y * b, this->_z * b); }
        inline Vector3 operator/(const double& b) const { return Vector3(this->_x / b, this->_y / b, this->_z / b); }

        inline Vector3 Normalize() const { return this->operator/(this->Length()); }
        inline double Length() const { return sqrt(_x * _x + _y * _y + _z * _z); }

        inline double operator*(const Vector3& b) const { return this->_x * b.GetX() + this->_y * b.GetY() + this->_z * b.GetZ(); }
        inline Vector3 operator^(const Vector3& b) const { return Vector3(this->_y * b.GetZ() - this->_z * b.GetY(), this->_z * b.GetX() - this->_x * b.GetZ(), this->_x * b.GetY() - this->_y * b.GetX()); }

    private:
        union
        {
            struct { double _x, _y, _z; };
            struct { double _cell[3]; };
        };
};

inline Vector3 operator+(const double& b, const Vector3& a) { return a + b; }
inline Vector3 operator-(const double& b, const Vector3& a) { return Vector3(b, b, b) - a; }
inline Vector3 operator*(const double& b, const Vector3& a) { return a * b; }
inline Vector3 operator/(const double& b, const Vector3& a) { return Vector3(b / a.GetX(), b / a.GetY(), b / a.GetZ()); }

inline std::ostream& operator<<(std::ostream& o, const Vector3& v)
{
    return o << "(" << v.GetX() << ", " << v.GetY() << ", " << v.GetZ() << ")";
}

#endif
