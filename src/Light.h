#ifndef LIGHT_HH
# define LIGHT_HH

#include "SceneObject.h"

class Light : public SceneObject
{
public:
    Light(ARG_TYPE_SCENE_OBJECT_WITH_TYPE, const Vector3& position);
    ~Light();

    const Vector3& Position() const;
    virtual double DiffuseLightEffect(const Ray& ray, const Vector3& intersectionPoint) const = 0;

protected:
    Vector3 _position;
};

#endif
