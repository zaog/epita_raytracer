#include "SceneObject.h"

SceneObject::SceneObject(ARG_TYPE_SCENE_OBJECT_WITH_TYPE)
    : _color(color), _type(type), _diffuse(diffuse), _specular(specular), _reflection(reflection), _refraction(refraction), _refractionIndex(refractionIndex)
{
    _isLight = false;
}

SceneObject::~SceneObject()
{}

std::ostringstream& SceneObject::Print(std::ostringstream& ss) const
{
    ss << "Color: " << _color << std::endl
        << "Diffuse: " << _diffuse << std::endl
        << "Specular: " << _specular << std::endl
        << "Reflection: " << _reflection << std::endl
        << "Refraction: " << _refraction << std::endl
        << "RefractionIndex: " << _refractionIndex << std::endl
        << "Is Light: " << _isLight;
    return ss;
}

std::ostream& operator<<(std::ostream& o, const SceneObject& sc)
{
    std::ostringstream oss;
    return o << sc.Print(oss).str();
}
