#include "Plane.h"

#include "AABB.h"
#include "Ray.h"
#include "Utils.h"

Plane::Plane(ARG_TYPE_SCENE_OBJECT, const Vector3& normal, const double& d)
    : SceneObject(ARG_SCENE_OBJECT_WITH_TYPE(ObjectType::PLANE)), _normal(normal.Normalize()), _d(d)
{}

Plane::~Plane()
{}

IntersectionType Plane::Intersection(const Ray& ray, double& distance) const
{
    double d = this->_normal * ray.Direction();

    if (std::abs(d) > EPSILON)
    {
        double tmpDistance = -(this->_normal * ray.Origin() + this->_d) / d;
        if (tmpDistance > 0)
        {
            if (tmpDistance < distance)
            {
                distance = tmpDistance;
                return HIT;
            }
        }
    }

    return MISS;
}

Vector3 Plane::Normal(const Vector3& point) const
{
    return _normal;
}

bool Plane::IntersectBox(const AABB& box) const
{
    Vector3 v[2];
    v[0] = box.GetPosition();
    v[1] = v[0] + box.GetSize();

    int side1 = 0, side2 = 0;
    for (int i = 0; i < 8; i++)
    {
        Vector3 p(v[i & 1].GetX(), v[(i >> 1) & 1].GetY(), v[(i >> 2) & 1].GetZ());
        if (p * _normal + _d < 0)
            side1++;
        else
            side2++;
    }

    if ((side1 == 0) || (side2 == 0))
        return false;
    else
        return true;
}

void Plane::CalculateRange(double& position1, double& position2, const unsigned int& axis) const
{
    position1 = -1000;
    position2 = 1000;
}

AABB Plane::GetAABB() const
{
    return AABB(Vector3(-10000, -10000, -10000), Vector3(20000, 20000, 20000));
}


std::ostringstream& Plane::Print(std::ostringstream& ss) const
{
    ss << "** Plane **" << std::endl
        << SceneObject::Print(ss).str() << std::endl
        << "Normal: " << _normal << std::endl
        << "Distance: " << _d;
    return ss;

}