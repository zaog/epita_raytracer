#ifndef PLANE_H
#define PLANE_H

#include "SceneObject.h"

class Plane : public SceneObject
{
public:
    Plane(ARG_TYPE_SCENE_OBJECT, const Vector3& normal, const double& d);
    ~Plane();

    IntersectionType Intersection(const Ray& ray, double& distance) const override;
    Vector3 Normal(const Vector3& point) const override;

    bool IntersectBox(const AABB& box) const override;
    void CalculateRange(double& position1, double& position2, const unsigned int& axis) const override;
    AABB GetAABB() const override;

    std::ostringstream& Print(std::ostringstream& ss) const;

private:
    Vector3 _normal;
    double _d;
};

#endif // PLANE_H
