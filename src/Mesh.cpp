#include "Mesh.h"

#include "ObjectsManager.h"

Mesh::Mesh(ARG_TYPE_SCENE_OBJECT, const std::vector<Vector3>& vertices, const std::vector<int>& triangles)
{
    for (unsigned int i = 0; i < triangles.size(); i += 3)
    {
        Triangle* triangle = new Triangle(ARG_SCENE_OBJECT, vertices[triangles[i]], vertices[triangles[i + 1]], vertices[triangles[i + 2]]);
        _triangles.push_back(triangle);
        ObjectsManager::Instance().AddObject(triangle);
    }
}

Mesh::~Mesh()
{}

std::ostringstream& Mesh::Print(std::ostringstream& ss) const
{
    ss << "** Mesh **" << std::endl;
    return ss;
}