#ifndef PIXEL_HH
# define PIXEL_HH

#include <iostream>

class Color
{
public:
    inline Color() : _r(0.0f), _g(0.0f), _b(0.0f) {}
    inline Color(const double& r, const double& g, const double& b) : _r(r), _g(g), _b(b) {}
    inline ~Color() {}

    inline const double& GetR() const { return _r; }
    inline const double& GetG() const { return _g; }
    inline const double& GetB() const { return _b; }
    inline Color& Set(const double& r, const double& g, const double& b) { this->_r = r; this->_g = g; this->_b = b; return *this; }

    inline Color& operator=(const Color& c) { return this->Set(c.GetR(), c.GetG(), c.GetB()); }

    inline Color& operator+=(const Color& b) { return this->Set(this->_r + b.GetR(), this->_g + b.GetG(), this->_b + b.GetB()); }
    inline Color& operator-=(const Color& b) { return this->Set(this->_r - b.GetR(), this->_g - b.GetG(), this->_b - b.GetB()); }
    inline Color& operator*=(const Color& b) { return this->Set(this->_r * b.GetR(), this->_g * b.GetG(), this->_b * b.GetB()); }
    inline Color& operator/=(const Color& b) { return this->Set(this->_r / b.GetR(), this->_g / b.GetG(), this->_b / b.GetB()); }

    inline Color operator+(const Color& b) const { return Color(this->_r + b.GetR(), this->_g + b.GetG(), this->_b + b.GetB()); }
    inline Color operator-(const Color& b) const { return Color(this->_r - b.GetR(), this->_g - b.GetG(), this->_b - b.GetB()); }
    inline Color operator*(const Color& b) const { return Color(this->_r * b.GetR(), this->_g * b.GetG(), this->_b * b.GetB()); }
    inline Color operator/(const Color& b) const { return Color(this->_r / b.GetR(), this->_g / b.GetG(), this->_b / b.GetB()); }

    inline Color& operator+=(const double& b) { return this->Set(this->_r + b, this->_g + b, this->_b + b); }
    inline Color& operator-=(const double& b) { return this->Set(this->_r - b, this->_g - b, this->_b - b); }
    inline Color& operator*=(const double& b) { return this->Set(this->_r * b, this->_g * b, this->_b * b); }
    inline Color& operator/=(const double& b) { return this->Set(this->_r / b, this->_g / b, this->_b / b); }

    inline Color operator+(const double& b) const { return Color(this->_r + b, this->_g + b, this->_b + b); }
    inline Color operator-(const double& b) const { return Color(this->_r - b, this->_g - b, this->_b - b); }
    inline Color operator*(const double& b) const { return Color(this->_r * b, this->_g * b, this->_b * b); }
    inline Color operator/(const double& b) const { return Color(this->_r / b, this->_g / b, this->_b / b); }

    double GetCheckedR() const;
    double GetCheckedG() const;
    double GetCheckedB() const;

private:
    double _r;
    double _g;
    double _b;
};

inline Color operator+(const double& b, const Color& a) { return a + b;  }
inline Color operator-(const double& b, const Color& a) { return Color(b, b, b) - a; }
inline Color operator*(const double& b, const Color& a) { return a * b; }
inline Color operator/(const double& b, const Color& a) { return Color(b / a.GetR(), b / a.GetG(), b / a.GetB()); }


inline std::ostream& operator<<(std::ostream& o, const Color& c)
{
    return o << "(" << c.GetR() << ", " << c.GetG() << ", " << c.GetB() << ")";
}

#endif
