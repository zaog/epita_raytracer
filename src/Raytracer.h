#ifndef RAYTRACER_HH
# define RAYTRACER_HH

#include <cmath>
#include <limits>
#include <vector>

#include "Vector3.h"

#include "CImg/CImg.h"

#define TILESIZE 32

using namespace cimg_library;

enum IntersectionType;

class Color;
class KDNode;
class Ray;
class SceneObject;

class Raytracer
{
public:
    Raytracer(const int& width, const int& height, const int& depth, const int& reflections, CImg<double>& image);
    ~Raytracer();

    CImg<double>& Image() const { return _image; }

    inline void addCentralDirection(const double& x, const double& y, const double& z) { _centralDirection += Vector3(x, y, z); }
    inline Vector3 copyContralDirection() const { return _centralDirection; }

    inline void increaseZoom() { _currentZoom *= 2; }
    inline void decreaseZoom() { _currentZoom /= 2; }

    void ComputeImage();

private:
    Color Raytrace(const Ray& ray, double& distance, std::vector<SceneObject*>::const_iterator& itObject, int numReflections, double refractionIndex);
    IntersectionType NearestIntersection(const Ray& ray, std::vector<SceneObject*>::const_iterator& itObject, double& distance);
    bool ComputeShadow(const Ray& ray, const std::vector<SceneObject*>::const_iterator& itObject, double distance);
    Ray ReflectedRay(const Ray& ray, const Vector3& normal, const Vector3& hitPoint);

private:
    int _width;
    int _height;
    int _depth;
    int _reflections;
    CImg<double>& _image;

    Vector3 _centralDirection;
    double _currentZoom;
    //Color** _image;

    class KDStack
    {
    public:
        KDStack() : node(nullptr), t(0.f), pb(), prev(-1) {}
        KDNode const* node;
        double t;
        Vector3 pb;
        int prev;
    };

    std::vector<KDStack> _stack;
};

#endif
