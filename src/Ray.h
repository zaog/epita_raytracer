#ifndef RAY_HH
# define RAY_HH

#include "Vector3.h"

class Ray
{
    public:
        Ray();
        Ray(const Vector3& position, const Vector3& direction);
        ~Ray();

        Ray& operator=(const Ray& r);

        const Vector3& Origin() const;
        const Vector3& Direction() const;

    private:
        Vector3 _origin;
        Vector3 _direction;
};

#endif
