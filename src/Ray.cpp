#include "Ray.h"

Ray::Ray()
    : _origin(), _direction()
{}

Ray::Ray(const Vector3& position, const Vector3& direction)
    : _origin(position), _direction(direction)
{}

Ray::~Ray()
{}

Ray& Ray::operator=(const Ray& r)
{
    _origin = r.Origin();
    _direction = r.Direction();

    return *this;
}

const Vector3& Ray::Origin() const
{
    return _origin;
}

const Vector3& Ray::Direction() const
{
    return _direction;
}
