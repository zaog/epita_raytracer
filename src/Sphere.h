#ifndef SPHERE_HH
# define SPHERE_HH

#include <vector>

#include "SceneObject.h"
#include "Vector3.h"

class Sphere : public SceneObject
{
    public:
        Sphere(ARG_TYPE_SCENE_OBJECT, const Vector3& position, const double& r);
        ~Sphere();

        IntersectionType Intersection(const Ray& ray, double& distance) const override;
        Vector3 Normal(const Vector3& point) const override;

        bool IntersectBox(const AABB& box) const override;
        void CalculateRange(double& position1, double& position2, const unsigned int& axis) const override;
        AABB GetAABB() const override;

        std::ostringstream& Print(std::ostringstream& ss) const;

    private:
        Vector3 _position;
        double _r;
};

#endif
