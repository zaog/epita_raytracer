#include "PonctualLight.h"

#include "AABB.h"
#include "Ray.h"

PonctualLight::PonctualLight(ARG_TYPE_SCENE_OBJECT, const Vector3& position)
    : Light(ARG_SCENE_OBJECT_WITH_TYPE(ObjectType::SPHERE), position), _r(0.1)
{}

PonctualLight::~PonctualLight()
{}

IntersectionType PonctualLight::Intersection(const Ray& ray, double& distance) const
{
    Vector3 v = ray.Origin() - this->_position;
    double b = -(v * ray.Direction());
    double delta = (b * b) - v * v + _r * _r;

    if (delta > 0)
    {
        delta = sqrt(delta);
        double i1 = b - delta;
        double i2 = b - delta;
        if (i2 > 0)
        {
            if (i1 < 0)
            {
                if (i2 < distance)
                {
                    distance = i2;
                    return INOBJECT;
                }
            }
            else
            {
                if (i1 < distance)
                {
                    distance = i1;
                    return HIT;
                }
            }
        }
    }

    return MISS;
}

Vector3 PonctualLight::Normal(const Vector3& point) const
{
    Vector3 v = point - _position;
    return v / _r;
}

bool PonctualLight::IntersectBox(const AABB& box) const
{
    double dmin = 0;
    for (int i = 0; i < 3; i++)
    {
        if (_position.GetCell(i) < box.GetPosition().GetCell(i))
        {
            dmin += std::pow(_position.GetCell(i) - box.GetPosition().GetCell(i), 2);
        }
        else if (_position.GetCell(i) > (box.GetPosition().GetCell(i) + box.GetSize().GetCell(i)))
        {
            dmin += std::pow(_position.GetCell(i) - (box.GetPosition().GetCell(i) + box.GetSize().GetCell(i)), 2);
        }
    }
    return (dmin <= _r);
}

void PonctualLight::CalculateRange(double& position1, double& position2, const unsigned int& axis) const
{
    position1 = _position.GetCell(axis) - _r;
    position2 = _position.GetCell(axis) + _r;
}

AABB PonctualLight::GetAABB() const
{
    Vector3 size(_r, _r, _r);
    return AABB(_position - size, size * 2);
}

double PonctualLight::DiffuseLightEffect(const Ray& ray, const Vector3& intersectionPoint) const
{
    return (_position - ray.Origin()).Normalize() * (intersectionPoint - _position).Normalize();
}
