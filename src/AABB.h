#pragma once

#include "Vector3.h"

class AABB
{
public:
    AABB() : _position(Vector3(0, 0, 0)), _size(Vector3(0, 0, 0)) {};
    AABB(const Vector3& position, const Vector3& size) : _position(position), _size(size) {};

    inline Vector3& GetPosition() { return _position; }
    inline Vector3& GetSize() { return _size; }
    inline const Vector3& GetPosition() const { return _position; }
    inline const Vector3& GetSize() const { return _size; }

    inline bool Intersect(AABB& b2)
    {
        Vector3 v1 = b2.GetPosition(), v2 = b2.GetPosition() + b2.GetSize();
        Vector3 v4 = _position + _size;
        return ((v4.GetX() >= v1.GetX()) && (_position.GetX() <= v2.GetX()) && // x-axis overlap
                (v4.GetY() >= v1.GetY()) && (_position.GetY() <= v2.GetY()) && // y-axis overlap
                (v4.GetZ() >= v1.GetZ()) && (_position.GetZ() <= v2.GetZ()));   // z-axis overlap
    }

    inline bool Contains(Vector3 position)
    {
        Vector3 v2 = _position + _size;
        return ((position.GetX() >= _position.GetX()) && (position.GetX() <= v2.GetX()) &&
                (position.GetY() >= _position.GetY()) && (position.GetY() <= v2.GetY()) &&
                (position.GetZ() >= _position.GetZ()) && (position.GetZ() <= v2.GetZ()));
    }

    double GetW() { return _size.GetX(); }
    double GetH() { return _size.GetY(); }
    double GetD() { return _size.GetZ(); }
    double GetX() { return _position.GetX(); }
    double GetY() { return _position.GetY(); }
    double GetZ() { return _position.GetZ(); }

private:
    Vector3 _position, _size;
};
