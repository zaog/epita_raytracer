#include "Color.h"
#include "Light.h"
#include "ObjectsManager.h"
#include "Plane.h"
#include "PonctualLight.h"
#include "SceneObject.h"
#include "Sphere.h"
#include "Triangle.h"
#include "Utils.h"

#include "Parser.h"

Parser::Parser(char const* path)
    : _file(path), _screen_width(0), _screen_height(0), _screen_depth(0), _reflections(1)
{
    _file.LoadFile();
}

Parser::~Parser()
{
}

void Parser::ParseFile()
{
    TiXmlNode const* actual = _file.FirstChild();

    do
    {
        if (actual->ValueTStr() == "Scene")
            this->ParseScene(actual->FirstChild());
        else if (actual->ValueTStr() == "Screen")
            this->ParseScreen(actual->FirstChild());
    } while ((actual = actual->NextSibling()) != nullptr);
}

void Parser::ParseScene(TiXmlNode const* actual)
{
    do
    {
        if (actual->ValueTStr() == "Sphere")
            this->ParseSphere(actual->FirstChild());
        else if (actual->ValueTStr() == "Plane")
            this->ParsePlane(actual->FirstChild());
        else if (actual->ValueTStr() == "Triangle")
            this->ParseTriangle(actual->FirstChild());
        else if (actual->ValueTStr() == "PonctualLight")
            this->ParsePonctualLight(actual->FirstChild());
        else if (actual->ValueTStr() == "For")
            this->ParseFor(actual->FirstChild());
    } while ((actual = actual->NextSibling()) != nullptr);
}

void Parser::ParseScreen(TiXmlNode const* actual)
{
    do
    {
        if (actual->ValueTStr() == "width")
            _screen_width = this->ParseInt(actual->FirstChild());
        else if (actual->ValueTStr() == "height")
            _screen_height = this->ParseInt(actual->FirstChild());
        else if (actual->ValueTStr() == "depth")
            _screen_depth = this->ParseInt(actual->FirstChild());
        else if (actual->ValueTStr() == "reflections")
            _reflections = this->ParseInt(actual->FirstChild());
        else if (actual->ValueTStr() == "p1")
            _p1Grid = this->ParseVector3(actual->FirstChild());
        else if (actual->ValueTStr() == "p2")
            _p2Grid = this->ParseVector3(actual->FirstChild());
    } while ((actual = actual->NextSibling()) != nullptr);
}

void Parser::ParseSphere(TiXmlNode const* actual)
{
    LIST_ARG_SCENE_OBJECT;
    Vector3 position;
    double r = 0;

    do
    {
        if PARSE_COLOR(color, color);
        else if PARSE_DOUBLE(diff, diffuse);
        else if PARSE_DOUBLE(spec, specular);
        else if PARSE_DOUBLE(refl, reflection);
        else if PARSE_DOUBLE(refr, refraction);
        else if PARSE_DOUBLE(refrIndex, refractionIndex);
        else if PARSE_VECTOR3(position, position);
        else if PARSE_DOUBLE(r, r);
    } while ((actual = actual->NextSibling()) != nullptr);

    ObjectsManager::Instance().AddObject(new Sphere(color, diff, spec, refl, refr, refrIndex, position, r));
}

void Parser::ParsePlane(TiXmlNode const* actual)
{
    LIST_ARG_SCENE_OBJECT;
    Vector3 normal;
    double d = 0;

    do
    {
        if PARSE_COLOR(color, color);
        else if PARSE_DOUBLE(diff, diffuse);
        else if PARSE_DOUBLE(spec, specular);
        else if PARSE_DOUBLE(refl, reflection);
        else if PARSE_DOUBLE(refr, refraction);
        else if PARSE_DOUBLE(refrIndex, refractionIndex);
        else if PARSE_VECTOR3(normal, normal);
        else if PARSE_DOUBLE(d, d);
    } while ((actual = actual->NextSibling()) != nullptr);

    ObjectsManager::Instance().AddObject(new Plane(color, diff, spec, refl, refr, refrIndex, normal, d));
}

void Parser::ParseTriangle(TiXmlNode const* actual)
{
    LIST_ARG_SCENE_OBJECT;
    Vector3 p0;
    Vector3 p1;
    Vector3 p2;

    do
    {
        if PARSE_COLOR(color, color);
        else if PARSE_DOUBLE(diff, diffuse);
        else if PARSE_DOUBLE(spec, specular);
        else if PARSE_DOUBLE(refl, reflection);
        else if PARSE_DOUBLE(refr, refraction);
        else if PARSE_DOUBLE(refrIndex, refractionIndex);
        else if PARSE_VECTOR3(p0, p0);
        else if PARSE_VECTOR3(p1, p1);
        else if PARSE_VECTOR3(p2, p2);
    } while ((actual = actual->NextSibling()) != nullptr);

    ObjectsManager::Instance().AddObject(new Triangle(color, diff, spec, refl, refr, refrIndex, p0, p1, p2));
}

void Parser::ParseMesh(TiXmlNode const* actual)
{
    LIST_ARG_SCENE_OBJECT;
    std::string path;

    do
    {
        if PARSE_COLOR(color, color);
        else if PARSE_DOUBLE(diff, diffuse);
        else if PARSE_DOUBLE(spec, specular);
        else if PARSE_DOUBLE(refl, reflection);
        else if PARSE_DOUBLE(refr, refraction);
        else if PARSE_DOUBLE(refrIndex, refractionIndex);
        else if PARSE_STRING(path, path);
    } while ((actual = actual->NextSibling()) != nullptr);

    ObjectsManager::Instance().AddMesh(new Mesh(color, diff, spec, refl, refr, refrIndex, std::vector<Vector3>(), std::vector<int>()));
}

void Parser::ParsePonctualLight(TiXmlNode const* actual)
{
    Vector3 position;
    Color color;

    do
    {
        if (actual->ValueTStr() == "position")
            position = this->ParseVector3(actual->FirstChild());
        else if (actual->ValueTStr() == "color")
            color = this->ParseColor(actual->FirstChild());
    } while ((actual = actual->NextSibling()) != nullptr);

    Light* light = new PonctualLight(color, 1, 0, 0, 0, 0, position);

    ObjectsManager::Instance().AddObject(light);
    ObjectsManager::Instance().AddLight(light);
}

Vector3 Parser::ParseVector3(TiXmlNode const* actual)
{
    double x = 0;
    double y = 0;
    double z = 0;

    do
    {
        if (actual->ValueTStr() == "x")
            x = this->ParseDouble(actual->FirstChild());
        else if (actual->ValueTStr() == "y")
            y = this->ParseDouble(actual->FirstChild());
        else if (actual->ValueTStr() == "z")
            z = this->ParseDouble(actual->FirstChild());
    } while ((actual = actual->NextSibling()) != nullptr);

    return Vector3(x, y, z);
}

Color Parser::ParseColor(TiXmlNode const* actual)
{
    double r = 0;
    double g = 0;
    double b = 0;

    do
    {
        if (actual->ValueTStr() == "r")
            r = this->ParseDouble(actual->FirstChild());
        else if (actual->ValueTStr() == "g")
            g = this->ParseDouble(actual->FirstChild());
        else if (actual->ValueTStr() == "b")
            b = this->ParseDouble(actual->FirstChild());
    } while ((actual = actual->NextSibling()) != nullptr);

    return Color(r, g, b);
}

double Parser::ParseDouble(TiXmlNode const* actual)
{
    TiXmlNode const* sibling = actual->NextSibling();
    double forAdditiveValue = 0;
    while (sibling != nullptr)
    {
        forAdditiveValue += _forValues[sibling->Value()];
        sibling = sibling->NextSibling();
    }
    return Utils::stod(actual->Value()) + forAdditiveValue;
}

int Parser::ParseInt(TiXmlNode const* actual)
{
    return Utils::stoi(actual->Value());
}

std::string Parser::ParseString(TiXmlNode const* actual)
{
    return actual->Value();
}

void Parser::ParseFor(TiXmlNode const* actual)
{
    std::string name;
    double init;
    double cond;
    double incr;

    actual->Parent()->ToElement()->QueryStringAttribute("name", &name);
    actual->Parent()->ToElement()->QueryDoubleAttribute("init", &init);
    actual->Parent()->ToElement()->QueryDoubleAttribute("cond", &cond);
    actual->Parent()->ToElement()->QueryDoubleAttribute("incr", &incr);

    _forValues.emplace(name, init);

    while (_forValues[name] < cond)
    {
        ParseScene(actual);
        _forValues[name] += incr;
    }

    _forValues.erase(name);
}