#ifndef SCENEOBJECT_HH
# define SCENEOBJECT_HH

#include <vector>
#include <iostream>
#include <sstream>

#include "Color.h"
#include "Vector3.h"

enum IntersectionType
{
    MISS = 0,
    HIT = 1,
    INOBJECT = -1
};

enum ObjectType
{
    UNDEF = 0,
    SPHERE,
    PLANE,
    TRIANGLE
};

class AABB;
class Ray;

#define ARG_TYPE_SCENE_OBJECT const Color& color, const double& diffuse, const double& specular, const double& reflection, const double& refraction, const double& refractionIndex
#define ARG_TYPE_SCENE_OBJECT_WITH_TYPE const Color& color, const ObjectType& type, const double& diffuse, const double& specular, const double& reflection, const double& refraction, const double& refractionIndex
#define ARG_SCENE_OBJECT color, diffuse, specular, reflection, refraction, refractionIndex
#define ARG_SCENE_OBJECT_WITH_TYPE(type) color, type, diffuse, specular, reflection, refraction, refractionIndex

class SceneObject
{
    public:
        SceneObject(ARG_TYPE_SCENE_OBJECT_WITH_TYPE);
        ~SceneObject();

        virtual IntersectionType Intersection(const Ray& ray, double& distance) const = 0;
        virtual Vector3 Normal(const Vector3& point) const = 0;

        virtual bool IntersectBox(const AABB& box) const = 0;
        virtual void CalculateRange(double& position1, double& position2, const unsigned int& axis) const = 0;
        virtual AABB GetAABB() const = 0;

        inline const Color& GetColor() const { return _color; }
        inline const ObjectType& GetType() const { return _type; }
        inline const double& GetDiffuse() const { return _diffuse; }
        inline const double& GetSpecular() const { return _specular; }
        inline const double& GetReflection() const { return _reflection; }
        inline const double& GetRefraction() const { return _refraction; }
        inline const double& GetRefractionIndex() const { return _refractionIndex; }

        inline const bool& IsLight() const { return _isLight; }

        std::ostringstream& Print(std::ostringstream& ss) const;

    protected:
        Color _color;
        ObjectType _type;
        double _diffuse;
        double _specular;
        double _reflection;
        double _refraction;
        double _refractionIndex;

        bool _isLight;
};

std::ostream& operator<<(std::ostream& o, const SceneObject& v);

#endif
