#include <iostream>

#include "ObjectsManager.h"
#include "Raytracer.h"

#include "Utils.h"

Raytracer::Raytracer(const int& width, const int& height, const int& depth, const int& reflections, CImg<double>& image)
    : _width(width), _height(height), _depth(depth), _reflections(reflections), _image(image), _centralDirection(0, 0, 0), _currentZoom(1)
{
}

Raytracer::~Raytracer()
{
}

void Raytracer::ComputeImage()
{
    Vector3 centralDirection = this->copyContralDirection();
    // screen plane in world space coordinates
    double WX1 = -8 * _currentZoom;
    double WX2 = 8 * _currentZoom;
    double WY1 = 6 * _currentZoom;
    double WY2 = -6 * _currentZoom;
    // calculate deltas for interpolation
    double DX = (WX2 - WX1) / _width;
    double DY = (WY2 - WY1) / _height;
    double SY = WY1 + 20 * DY;

    auto itLastObject = ObjectsManager::Instance().Objects().cend();

    cimg_forY(_image, j)
    {
        double SX = WX1;

        cimg_forX(_image, i)
        {
            Vector3 positionRay = Vector3(centralDirection.GetX(), centralDirection.GetY(), -_depth);
            Vector3 directionRay = Vector3(centralDirection.GetX() + SX, centralDirection.GetY() + SY, _depth).Normalize();
            Ray ray = Ray(positionRay, directionRay);

            double distance = 1048576.0f;
            auto itObject = ObjectsManager::Instance().Objects().cend();
            Color p = Raytrace(ray, distance, itObject, _reflections, 1.f);
            if (itLastObject != itObject)
            {
                itLastObject = itObject;
                Color acc(0, 0, 0);
                for (int tx = -1; tx < 2; tx++) for (int ty = -1; ty < 2; ty++)
                {
                    directionRay = Vector3(SX + DX * tx / 2.0f, SY + DY * ty / 2.0f, _depth).Normalize();
                    ray = Ray(positionRay, directionRay);
                    double distance = 1048576.0f;
                    auto itObject = ObjectsManager::Instance().Objects().cend();
                    acc += Raytrace(ray, distance, itObject, _reflections, 1.f);
                }
                p.Set(acc.GetR() / 9, acc.GetG() / 9, acc.GetB() / 9);
            }

            _image(i, j, 0) = p.GetR() * 255;
            _image(i, j, 1) = p.GetG() * 255;
            _image(i, j, 2) = p.GetB() * 255;

            SX += DX;
        }

        SY += DY;
    }
}

Color Raytracer::Raytrace(const Ray& ray, double& distance, std::vector<SceneObject*>::const_iterator& itObject, int numReflections, double refractionIndex)
{
    Color pixel = Color();

    int result = 0;

    if ((result = NearestIntersection(ray, itObject, distance)) != MISS)
    {
        Vector3 intersectionPoint = ray.Origin() + ray.Direction() * distance;
        Vector3 normal = (*itObject)->Normal(intersectionPoint);

        if ((*itObject)->IsLight())
        {
            Light const* lightObject = (Light*)(*itObject);
            double scalar = lightObject->DiffuseLightEffect(ray, intersectionPoint);
            if (scalar < 0)
            {
                pixel += (*itObject)->GetColor() * pow(-scalar, 3);
                double distanceLightEffect = 1048576.0f;
                auto it = ObjectsManager::Instance().Objects().cend();
                pixel += Raytrace(Ray(intersectionPoint + ray.Direction() * EPSILON, ray.Direction()), distanceLightEffect, it, numReflections, refractionIndex);
            }
        }
        else
        {
            for (auto it = ObjectsManager::Instance().Lights().cbegin(); it != ObjectsManager::Instance().Lights().cend(); it++)
            {
                Vector3 vectorObjectLight = (*it)->Position() - intersectionPoint;
                double distanceObjectLight = vectorObjectLight.Length();
                Vector3 directionObjectLight = vectorObjectLight / distanceObjectLight;

                if (ComputeShadow(Ray(intersectionPoint + directionObjectLight * EPSILON, directionObjectLight), itObject, distanceObjectLight))
                    continue;

                if ((*itObject)->GetDiffuse() > 0)
                {
                    double scalar = normal * directionObjectLight;
                    if (scalar > 0)
                        pixel += (*it)->GetColor() * (*itObject)->GetColor() * (*itObject)->GetDiffuse() * scalar;
                }

                if ((*itObject)->GetSpecular() > 0)
                {
                    Ray reflectedLight = ReflectedRay(Ray((*it)->Position() + directionObjectLight * EPSILON, directionObjectLight), normal, intersectionPoint);
                    double scalar = ray.Direction() * reflectedLight.Direction();
                    if (scalar > 0)
                        pixel += (*it)->GetColor() * pow(scalar, 20) * (*itObject)->GetSpecular();
                }
            }
            //pixel += (*itObject)->GetColor() * 0.04;

            if (numReflections > 0 && (*itObject)->GetReflection() > 0)
            {
                Ray reflectedRay = ReflectedRay(ray, normal, intersectionPoint);
                double distanceReflection = 1048576.0f;
                auto itObjectReflected = ObjectsManager::Instance().Objects().cend();
                pixel += Raytrace(reflectedRay, distanceReflection, itObjectReflected, numReflections - 1, refractionIndex) * (*itObject)->GetReflection() * (*itObject)->GetColor();
            }

            if ((*itObject)->GetRefraction() > 0)
            {
                double n = refractionIndex / (*itObject)->GetRefractionIndex();
                Vector3 refractedNormal = normal * result;
                double cosI = -1 * refractedNormal * ray.Direction();
                double cosT2 = 1.f - n * n * (1.f - cosI * cosI);
                if (cosT2 > 0.f)
                {
                    Vector3 T = (n * ray.Direction()) + (n * cosI - sqrt(cosT2)) * refractedNormal;
                    double distanceRefraction = 1048576.0f;
                    auto itObjectRefracted = ObjectsManager::Instance().Objects().cend();
                    Color refractionColor = Raytrace(Ray(intersectionPoint + T * EPSILON, T), distanceRefraction, itObjectRefracted, numReflections - 1, refractionIndex) * (*itObject)->GetRefraction();
                    Color absorbance = -1 * (*itObject)->GetColor() * 0.15f * distanceRefraction;
                    Color transparency = Color(exp(absorbance.GetR()), exp(absorbance.GetG()), exp(absorbance.GetB()));
                    pixel += refractionColor * transparency;
                }
            }
        }
    }

    return pixel;
}

IntersectionType Raytracer::NearestIntersection(const Ray& ray, std::vector<SceneObject*>::const_iterator& itObject, double& distance)
{
    IntersectionType result = MISS;
    for (auto it = ObjectsManager::Instance().Objects().cbegin(); it != ObjectsManager::Instance().Objects().cend(); it++)
    {
        IntersectionType tmpResult = (*it)->Intersection(ray, distance);
        if (tmpResult != MISS)
        {
            result = tmpResult;
            itObject = it;
        }
    }

    return result;
}

bool Raytracer::ComputeShadow(const Ray& ray, const std::vector<SceneObject*>::const_iterator& itObject, double distance)
{
    for (auto it = ObjectsManager::Instance().Objects().cbegin(); it != ObjectsManager::Instance().Objects().cend(); it++)
    {
        if (it != itObject && !(*it)->IsLight())
        {
            IntersectionType result = (*it)->Intersection(ray, distance);
            if (result != MISS)
                return true;
        }
    }

    return false;
}

Ray Raytracer::ReflectedRay(const Ray& ray, const Vector3& normal, const Vector3& hitPoint)
{
    Vector3 r = ray.Direction() - (normal * (normal * ray.Direction()) * 2);

    return Ray(hitPoint + r * EPSILON, r);
}
