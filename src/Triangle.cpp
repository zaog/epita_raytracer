#include "Triangle.h"

#include "AABB.h"
#include "Ray.h"
#include "Utils.h"

Triangle::Triangle(ARG_TYPE_SCENE_OBJECT, const Vector3& p0, const Vector3& p1, const Vector3& p2)
    : SceneObject(ARG_SCENE_OBJECT_WITH_TYPE(ObjectType::TRIANGLE)), _p0(p0), _p1(p1), _p2(p2)
{
    _v0v1 = _p1 - _p0;
    _v0v2 = _p2 - _p0;

    _normal = _v0v1 ^ _v0v2;

    if (std::abs(_normal.GetX()) > std::abs(_normal.GetY()))
        if (std::abs(_normal.GetX()) > std::abs(_normal.GetZ()))
            _k = 0;
        else
            _k = 2;
    else
        if (std::abs(_normal.GetY()) > std::abs(_normal.GetZ()))
            _k = 1;
        else
            _k = 2;

    _ku = (_k + 1) % 3;
    _kv = (_k + 2) % 3;

    double krec = 1.0f / _normal.GetCell(_k);
    _nu = _normal.GetCell(_ku) * krec;
    _nv = _normal.GetCell(_kv) * krec;
    _nd = _normal * _p0 * krec;

    double reci = 1.0f / (_v0v1.GetCell(_ku) * _v0v2.GetCell(_kv) - _v0v1.GetCell(_kv) * _v0v2.GetCell(_ku));
    _bnu = _v0v1.GetCell(_ku) * reci;
    _bnv = -_v0v1.GetCell(_kv) * reci;

    _cnu =  _v0v2.GetCell(_kv) * reci;
    _cnv = -_v0v2.GetCell(_ku) * reci;

    _normal = _normal.Normalize();
}

Triangle::~Triangle()
{}

IntersectionType Triangle::Intersection(const Ray& ray, double& distance) const
{
    const Vector3& origin = ray.Origin();
    const Vector3& direction = ray.Direction();

    const double lnd = 1.0f / (direction.GetCell(_k) + _nu * direction.GetCell(_ku) + _nv * direction.GetCell(_kv));
    const double tmpDistance = (_nd - origin.GetCell(_k) - _nu * origin.GetCell(_ku) - _nv * origin.GetCell(_kv)) * lnd;

    if (!(distance > tmpDistance && tmpDistance > 0))
        return MISS;

    double hu = origin.GetCell(_ku) + tmpDistance * direction.GetCell(_ku) - _p0.GetCell(_ku);
    double hv = origin.GetCell(_kv) + tmpDistance * direction.GetCell(_kv) - _p0.GetCell(_kv);

    double u = hv * _bnu + hu * _bnv;
    if (u < 0)
        return MISS;

    double v = hu * _cnu + hv * _cnv;
    if (v < 0)
        return MISS;

    if ((u + v) > 1)
        return MISS;

    distance = tmpDistance;
    return ((direction * _normal) > 0) ? INOBJECT : HIT;
}

Vector3 Triangle::Normal(const Vector3& point) const
{
    return _normal;
}

bool Triangle::IntersectBox(const AABB& box) const
{
    Vector3 boxHalfSize = box.GetSize() / 2;
    Vector3 boxCenter = box.GetPosition() + boxHalfSize;
    Vector3 v0, v1, v2, normal, e0, e1, e2;
    float min, max, p0, p1, p2, rad, fex, fey, fez;
    v0 = _p0 - boxCenter;
    v1 = _p1 - boxCenter;
    v2 = _p2 - boxCenter;
    e0 = v1 - v0, e1 = v2 - v1, e2 = v0 - v2;
    fex = fabsf(e0.GetCell(0));
    fey = fabsf(e0.GetCell(1));
    fez = fabsf(e0.GetCell(2));
    AXISTEST_X01(e0.GetCell(2), e0.GetCell(1), fez, fey);
    AXISTEST_Y02(e0.GetCell(2), e0.GetCell(0), fez, fex);
    AXISTEST_Z12(e0.GetCell(1), e0.GetCell(0), fey, fex);
    fex = fabsf(e1.GetCell(0));
    fey = fabsf(e1.GetCell(1));
    fez = fabsf(e1.GetCell(2));
    AXISTEST_X01(e1.GetCell(2), e1.GetCell(1), fez, fey);
    AXISTEST_Y02(e1.GetCell(2), e1.GetCell(0), fez, fex);
    AXISTEST_Z0(e1.GetCell(1), e1.GetCell(0), fey, fex);
    fex = fabsf(e2.GetCell(0));
    fey = fabsf(e2.GetCell(1));
    fez = fabsf(e2.GetCell(2));
    AXISTEST_X2(e2.GetCell(2), e2.GetCell(1), fez, fey);
    AXISTEST_Y1(e2.GetCell(2), e2.GetCell(0), fez, fex);
    AXISTEST_Z12(e2.GetCell(1), e2.GetCell(0), fey, fex);
    FINDMINMAX(v0.GetCell(0), v1.GetCell(0), v2.GetCell(0), min, max);
    if (min > boxHalfSize.GetCell(0) || max < -boxHalfSize.GetCell(0)) return false;
    FINDMINMAX(v0.GetCell(1), v1.GetCell(1), v2.GetCell(1), min, max);
    if (min > boxHalfSize.GetCell(1) || max < -boxHalfSize.GetCell(1)) return false;
    FINDMINMAX(v0.GetCell(2), v1.GetCell(2), v2.GetCell(2), min, max);
    if (min > boxHalfSize.GetCell(2) || max < -boxHalfSize.GetCell(2)) return false;
    normal = e0 ^ e1;

    Vector3 vmin, vmax;
    for (int q = 0; q < 3; q++)
    {
        float v = v0.GetCell(q);
        if (normal.GetCell(q) > 0.0f)
        {
            vmin.SetCell(q, -boxHalfSize.GetCell(q) - v);
            vmax.SetCell(q, boxHalfSize.GetCell(q) - v);
        }
        else
        {
            vmin.SetCell(q, boxHalfSize.GetCell(q) - v);
            vmax.SetCell(q, -boxHalfSize.GetCell(q) - v);
        }
    }
    if (normal * vmin > 0.0f)
        return true;
    if (normal * vmax >= 0.0f)
        return false;
    return true;
}

void Triangle::CalculateRange(double& position1, double& position2, const unsigned int& axis) const
{
    position1 = _p0.GetCell(axis);
    position2 = _p0.GetCell(axis);
    for (int i = 1; i < 3; i++)
    {
        if (_p[i].GetCell(axis) < position1)
            position1 = _p[i].GetCell(axis);
        if (_p[i].GetCell(axis) > position2)
            position2 = _p[i].GetCell(axis);
    }
}

AABB Triangle::GetAABB() const
{
    Vector3 position = _p0;
    Vector3 v2 = _p0;
    for (unsigned int i = 1; i < 3; i++)
    {
        if (_p[i].GetX() < position.GetX())
            position.SetCell(0, _p[i].GetX());
        if (_p[i].GetY() < position.GetY())
            position.SetCell(1, _p[i].GetY());
        if (_p[i].GetZ() < position.GetZ())
            position.SetCell(2, _p[i].GetZ());
        if (_p[i].GetX() > v2.GetX())
            v2.SetCell(0, _p[i].GetX());
        if (_p[i].GetY() > v2.GetY())
            v2.SetCell(1, _p[i].GetY());
        if (_p[i].GetZ() > v2.GetZ())
            v2.SetCell(2, _p[i].GetZ());
    }

    return AABB(position, v2 - position);
}

std::ostringstream& Triangle::Print(std::ostringstream& ss) const
{
    ss << "** Triangle **" << std::endl
        << SceneObject::Print(ss).str() << std::endl
        << "P0: " << _p0 << std::endl
        << "P1: " << _p1 << std::endl
        << "P2: " << _p2 << std::endl;
    return ss;
}