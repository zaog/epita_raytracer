#include "Light.h"

Light::Light(ARG_TYPE_SCENE_OBJECT_WITH_TYPE, const Vector3& position)
    : SceneObject(ARG_SCENE_OBJECT_WITH_TYPE(type)), _position(position)
{
    _isLight = true;
}

Light::~Light()
{
}

const Vector3& Light::Position() const
{
    return _position;
}
