#include "Color.h"

double Color::GetCheckedR() const
{
    double r = _r;
    if (r < 0)
        r = 0;
    else if (r > 1)
        r = 1;
    return r;
}

double Color::GetCheckedG() const
{
    double g = _g;
    if (g < 0)
        g = 0;
    else if (g > 1)
        g = 1;
    return g;
}

double Color::GetCheckedB() const
{
    double b = _b;
    if (b < 0)
        b = 0;
    else if (b > 1)
        b = 1;
    return b;
}
