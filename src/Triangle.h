#ifndef TRIANGLE_H
# define TRIANGLE_H

#include "SceneObject.h"

#define FINDMINMAX( x0, x1, x2, min, max ) \
  min = max = x0; if(x1<min) min=x1; if(x1>max) max=x1; if(x2<min) min=x2; if(x2>max) max=x2;
// X-tests
#define AXISTEST_X01( a, b, fa, fb )											\
	p0 = a * v0.GetCell(1) - b * v0.GetCell(2), p2 = a * v2.GetCell(1)- b * v2.GetCell(2); \
    if (p0 < p2) { min = p0; max = p2;} else { min = p2; max = p0; }			\
	rad = fa * boxHalfSize.GetCell(1) + fb * boxHalfSize.GetCell(2);				\
	if (min > rad || max < -rad) return 0;
#define AXISTEST_X2( a, b, fa, fb )												\
	p0 = a * v0.GetCell(1) - b * v0.GetCell(2), p1 = a * v1.GetCell(1) - b * v1.GetCell(2);	\
    if (p0 < p1) { min = p0; max = p1; } else { min = p1; max = p0;}			\
	rad = fa * boxHalfSize.GetCell(1) + fb * boxHalfSize.GetCell(2);				\
	if(min>rad || max<-rad) return 0;
// Y-tests
#define AXISTEST_Y02( a, b, fa, fb )											\
	p0 = -a * v0.GetCell(0) + b * v0.GetCell(2), p2 = -a * v2.GetCell(0) + b * v2.GetCell(2); \
    if(p0 < p2) { min = p0; max = p2; } else { min = p2; max = p0; }			\
	rad = fa * boxHalfSize.GetCell(0) + fb * boxHalfSize.GetCell(2);				\
	if (min > rad || max < -rad) return 0;
#define AXISTEST_Y1( a, b, fa, fb )												\
	p0 = -a * v0.GetCell(0) + b * v0.GetCell(2), p1 = -a * v1.GetCell(0) + b * v1.GetCell(2); \
    if (p0 < p1) { min = p0; max = p1; } else { min = p1; max = p0; }			\
	rad = fa * boxHalfSize.GetCell(0) + fb * boxHalfSize.GetCell(2);				\
	if (min > rad || max < -rad) return 0;
// Z-tests
#define AXISTEST_Z12( a, b, fa, fb )											\
	p1 = a * v1.GetCell(0) - b * v1.GetCell(1), p2 = a * v2.GetCell(0) - b * v2.GetCell(1); \
    if(p2 < p1) { min = p2; max = p1; } else { min = p1; max = p2; }			\
	rad = fa * boxHalfSize.GetCell(0) + fb * boxHalfSize.GetCell(1);				\
	if (min > rad || max < -rad) return 0;
#define AXISTEST_Z0( a, b, fa, fb )												\
	p0 = a * v0.GetCell(0) - b * v0.GetCell(1), p1 = a * v1.GetCell(0) - b * v1.GetCell(1);	\
    if(p0 < p1) { min = p0; max = p1; } else { min = p1; max = p0; }			\
	rad = fa * boxHalfSize.GetCell(0) + fb * boxHalfSize.GetCell(1);				\
	if (min > rad || max < -rad) return 0;


class Triangle : public SceneObject
{
public:
    Triangle(ARG_TYPE_SCENE_OBJECT, const Vector3& p0, const Vector3& p1, const Vector3& p2);
    ~Triangle();

    IntersectionType Intersection(const Ray& ray, double& distance) const override;
    Vector3 Normal(const Vector3& point) const override;

    bool IntersectBox(const AABB& box) const override;
    void CalculateRange(double& position1, double& position2, const unsigned int& axis) const override;
    AABB GetAABB() const override;

    std::ostringstream& Print(std::ostringstream& ss) const;

private:
    union
    {
        struct { Vector3 _p0, _p1, _p2; };
        struct { Vector3 _p[3]; };
    };

    Vector3 _v0v1;
    Vector3 _v0v2;

    Vector3 _normal;

    int _k;
    int _ku, _kv;

    double _nu, _nv, _nd;
    double _bnu, _bnv;
    double _cnu, _cnv;
};

#endif
