#include "Utils.h"

int Utils::stoi(const std::string& str)
{
    int res = 0, sign = 1;
    unsigned int i = 0;

    if (str[i] == '-')
    {
        sign = -1;
        i++;
    }

    for (; i < str.size(); i++)
    {
        res *= 10;
        res += str[i] - '0';
    }

    return res * sign;
}

double Utils::stod(const std::string& str)
{
    double res = 0;
    double factor = 1;
    unsigned int i = 0;
    int sign = 1;

    bool decimal = false;

    if (str[i] == '-')
    {
        sign = -1;
        i++;
    }

    for (; i < str.size(); i++)
    {
        if (str[i] == '.')
        {
            decimal = true;
        }
        else if (decimal)
        {
            factor /= 10;
            res += (str[i] - '0') * factor;
        }
        else
        {
            res *= 10;
            res += str[i] - '0';
        }
    }

    return res * sign;
}