#ifndef MESH_H
#define MESH_H

#include "SceneObject.h"
#include "Triangle.h"

class Mesh
{
public:
    Mesh(ARG_TYPE_SCENE_OBJECT, const std::vector<Vector3>& vertices, const std::vector<int>& triangles);
    ~Mesh();

    std::ostringstream& Print(std::ostringstream& ss) const;

private:
    std::vector<Triangle*> _triangles;
};


#endif // MESH_H