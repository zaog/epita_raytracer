#ifndef PARSER_HH
# define PARSER_HH

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <map>

#include "Parser/tinyxml.h"
#include "Vector3.h"

class Color;

#define PARSE_DOUBLE(var, name) \
    (actual->ValueTStr() == #name) \
        var = this->ParseDouble(actual->FirstChild())
#define PARSE_COLOR(var, name) \
    (actual->ValueTStr() == #name) \
        var = this->ParseColor(actual->FirstChild())
#define PARSE_VECTOR3(var, name) \
    (actual->ValueTStr() == #name) \
        var = this->ParseVector3(actual->FirstChild())
#define PARSE_STRING(var, name) \
    (actual->ValueTStr() == #name) \
        var = this->ParseString(actual->FirstChild())
#define LIST_ARG_SCENE_OBJECT \
    Color color; \
    double diff = 1; \
    double spec = 0; \
    double refl = 0; \
    double refr = 0; \
    double refrIndex = 1


class Parser
{
public:
    Parser(char const* path);
    ~Parser();

    void ParseFile();

    inline const int& GetScreenWidth() const { return _screen_width; }
    inline const int& GetScreenHeight() const { return _screen_height; }
    inline const int& GetScreenDepth() const { return _screen_depth; }
    inline const int& GetReflections() const { return _reflections; }

    inline const Vector3& GetP1() const { return _p1Grid; }
    inline const Vector3& GetP2() const { return _p2Grid; }

private:
    // Global parse functions
    void ParseScene(TiXmlNode const* actual);
    void ParseScreen(TiXmlNode const* actual);

    // Objects parse functions
    void ParseSphere(TiXmlNode const* actual);
    void ParsePlane(TiXmlNode const* actual);
    void ParseTriangle(TiXmlNode const* actual);
    void ParseMesh(TiXmlNode const* actual);
        
    // Lights parse functions
    void ParsePonctualLight(TiXmlNode const* actual);

    // Fields parse function
    Vector3 ParseVector3(TiXmlNode const* actual);
    Color ParseColor(TiXmlNode const* actual);
    double ParseDouble(TiXmlNode const* actual);
    int ParseInt(TiXmlNode const* actual);
    std::string ParseString(TiXmlNode const* actual);

    void ParseFor(TiXmlNode const* actual);

private:
    TiXmlDocument _file;

    std::map<std::string, double> _forValues;

    // Screen properties
    int _screen_width;
    int _screen_height;
    int _screen_depth;
    int _reflections;

    Vector3 _p1Grid, _p2Grid;
};

#endif
