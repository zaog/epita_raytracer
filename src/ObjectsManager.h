#ifndef OBJECTS_MANAGER_H
#define OBJECTS_MANAGER_H

#include <algorithm>
#include <list>

#include "Light.h"
#include "SceneObject.h"
#include "Mesh.h"

#include "AABB.h"

#define MAXTREEDEPTH 20

class KDNode
{
public:
    KDNode(const std::vector<SceneObject*>& objects) : _axis(0), _left(nullptr), _right(nullptr), _position(0), _leaf(true), _objects(objects) {};
    ~KDNode() { delete _left; delete _right; }

    void SetAxis(const unsigned int& axis) { _axis = axis; }
    void SetSplitPos(const double& position) { _position = position; }

    inline const unsigned int& GetAxis() const { return _axis; }
    inline const double& GetSplitPosition() const { return _position; }

    void SetLeft(KDNode* left) { _left = left; }
    void SetRight(KDNode* right) { _right = right; }

    KDNode const* GetLeft() const { return _left; }
    KDNode const* GetRight() const { return _right; }

    void SetList(std::vector<SceneObject*> objects) { _objects = objects; }
    void Add(SceneObject* object) { _objects.push_back(object); }
    inline const std::vector<SceneObject*>& GetList() const { return _objects; }

    inline const bool& IsLeaf() const { return _leaf; }
    void SetLeaf(const bool& leaf) { _leaf = leaf; }

private:
    unsigned int _axis;
    KDNode *_left, *_right;
    double _position;
    bool _leaf;

    std::vector<SceneObject*> _objects;
};

class ObjectsManager
{
private:
    ObjectsManager() : _objects(), _meshes(), _lights() {};
    ~ObjectsManager();

public:
    static inline ObjectsManager& Instance()
    {
        static ObjectsManager* instance = nullptr;
        if (instance == nullptr)
            instance = new ObjectsManager();

        return *instance;
    }

    void InitiateKDTree();

private:
    void InsertSplitPosition(double position);
    void BuildKDTree(KDNode* node, AABB box, unsigned int depth);

    struct SplitPosition
    {
        double splitPosition;
        unsigned int n1count, n2count;
    };

    std::list<SplitPosition> _splitList;

public:
    inline const std::vector<SceneObject*>& Objects() const { return this->_objects; }
    inline const std::vector<Mesh*>& Meshes() const { return this->_meshes; }
    inline const std::vector<Light*>& Lights() const { return this->_lights; }

    inline const AABB& Extends() const { return this->_extends; }
    inline KDNode const* Root() const { return this->_root; }

    inline void AddObject(SceneObject* object) { _objects.push_back(object); }
    inline void AddMesh(Mesh* mesh) { _meshes.push_back(mesh); }
    inline void AddLight(Light* light) { _lights.push_back(light); }

    inline void SetExtends(const Vector3& p1, const Vector3& p2) { _extends = AABB(p1, p2 - p1); }

    inline void RemoveObject(SceneObject* object) { _objects.erase(std::remove(_objects.begin(), _objects.end(), object), _objects.end()); }
    inline void RemoveMesh(Mesh* mesh) { _meshes.erase(std::remove(_meshes.begin(), _meshes.end(), mesh), _meshes.end()); }
    inline void RemoveLight(Light* light) { _lights.erase(std::remove(_lights.begin(), _lights.end(), light), _lights.end()); }

private:
    std::vector<SceneObject*> _objects;
    std::vector<Mesh*> _meshes;
    std::vector<Light*> _lights;

    AABB _extends;
    KDNode* _root;
};

#endif // OBJECTS_MANAGER_H