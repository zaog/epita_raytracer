#ifndef PONCTUALLIGHT_HH
# define PONCTUALLIGHT_HH

#include "Light.h"

class PonctualLight : public Light
{
public:
    PonctualLight(ARG_TYPE_SCENE_OBJECT, const Vector3& positio);
    ~PonctualLight();

    virtual IntersectionType Intersection(const Ray& ray, double& distance) const override;
    virtual Vector3 Normal(const Vector3& point) const override;
    virtual double DiffuseLightEffect(const Ray& ray, const Vector3& intersectionPoint) const override;

    virtual bool IntersectBox(const AABB& box) const override;
    virtual void CalculateRange(double& position1, double& position2, const unsigned int& axis) const override;
    virtual AABB GetAABB() const override;

private:
    double _r;
};

#endif
