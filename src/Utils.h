#ifndef UTILS_H
#define UTILS_H

#include <string>

#define EPSILON 0.0000001f

namespace Utils
{
int stoi(const std::string& str);
double stod(const std::string& str);
}

#endif // UTILS_H