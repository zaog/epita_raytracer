#include <fstream>
#include <iostream>
#include <ctime>

#include "Parser.h"
#include "Raytracer.h"
#include "ObjectsManager.h"

#include "CImg/CImg.h"

#define FRAME_PER_SECOND 60

using namespace cimg_library;

void save_image(std::string filename, int width, int height, CImg<double> image)
{
    std::ofstream file;
    file.open(filename);

    file << "P3\n";
    file << width << " " << height << "\n";
    file << "255\n";
    for (int j = 0; j < height; j++)
    {
        for (int i = 0; i < width; i++)
        {
            file << image(i, j, 0) << " ";
            file << image(i, j, 1) << " ";
            file << image(i, j, 2);

            if (i != width - 1)
                file << " ";
        }
        file << "\n";
    }

    file.close();
}

void printObjects()
{
    for (auto it = ObjectsManager::Instance().Objects().begin(); it != ObjectsManager::Instance().Objects().end(); it++)
    {
        std::cout << (**it) << std::endl << "======" << std::endl;
    }
}

int main(int argc, char* argv[])
{
    if (argc == 3)
    {
        Parser parser(argv[1]);
        parser.ParseFile();

        int width = parser.GetScreenWidth();
        int height = parser.GetScreenHeight();

        printObjects();

        CImg<double> image(width, height, 1, 3, 0);
        CImgDisplay imageDisplay(image, argv[1]);

        Raytracer* rt = new Raytracer(width, height, parser.GetScreenDepth(), parser.GetReflections(), image);

        double timeToWait = 1000 / FRAME_PER_SECOND;

#pragma omp parallel sections
        {
#pragma omp section
        {
            int lastKey = imageDisplay.key();
            while (!imageDisplay.is_closed())
            {
                if (!imageDisplay.is_keySHIFTLEFT())
                {
                    if (imageDisplay.is_keyARROWUP())
                        rt->addCentralDirection(0, 0.1f, 0);
                    if (imageDisplay.is_keyARROWDOWN())
                        rt->addCentralDirection(0, -0.1f, 0);
                    if (imageDisplay.is_keyARROWLEFT())
                        rt->addCentralDirection(-0.1f, 0, 0);
                    if (imageDisplay.is_keyARROWRIGHT())
                        rt->addCentralDirection(0.1f, 0, 0);
                }
                else if (imageDisplay.key() != lastKey)
                {
                    if (imageDisplay.is_keyARROWUP())
                        rt->decreaseZoom();
                    if (imageDisplay.is_keyARROWDOWN())
                        rt->increaseZoom();
                    if (imageDisplay.is_keyS())
                        save_image(argv[2], width, height, image);
                }

                lastKey = imageDisplay.key();
                imageDisplay.wait(timeToWait);
            }
        }
#pragma omp section
        {
            while (!imageDisplay.is_closed())
            {
                const clock_t begin_time = clock();
                rt->ComputeImage();
                std::cout << double(clock() - begin_time) / CLOCKS_PER_SEC << " seconds." << std::endl;
                imageDisplay.display(image);
            }
        }
        }

        //save_image(argv[2], width, height, rt->Image());

        delete rt;
    }
    else
        std::cout << "Usage: ./raytracer scene file\n";

    return 0;
}
